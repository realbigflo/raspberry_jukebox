cmake_minimum_required(VERSION 3.1.3)

project(raspberry_jukebox)
enable_testing()

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include(cmake_download_googletest.txt)
add_subdirectory(proto)

# googletest & protobuf might not compile with -Wall -Wextra -Werror
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror -pedantic")

add_subdirectory(common)
add_subdirectory(provider)
add_subdirectory(jukebox)
add_subdirectory(tests)
