#include <gtest/gtest.h>
#include <raspberry_jukebox/config.h>

#include "rpi_jukebox_config.pb.h"
#include "rpi_jukebox_events.pb.h"

#include <experimental/filesystem>

using namespace raspberry_jukebox::config;

bool operator!=(const rpi_jukebox_events::EventAction& lhs, const rpi_jukebox_events::EventAction& rhs) {
	if (lhs.action() != rhs.action())
		return true;

	if (lhs.event().uid() != rhs.event().uid())
		return true;

	if (lhs.event().rfid_uid() != rhs.event().rfid_uid())
		return true;

	if (lhs.working_directory() != rhs.working_directory())
		return true;

	if (lhs.filename() != rhs.filename())
		return true;

	return false;
}

bool operator==(const rpi_jukebox_events::EventAction& lhs, const rpi_jukebox_events::EventAction& rhs) {
	return !(lhs != rhs);
}

bool operator==(const rpi_jukebox_config::Config& lhs, const rpi_jukebox_config::Config& rhs) {
	if (lhs.zmq_uri() != rhs.zmq_uri())
		return false;

	if (lhs.event_actions().size() != rhs.event_actions().size())
		return false;

	for (auto i = 0; i < lhs.event_actions().size(); ++i) {
		if (lhs.event_actions().Get(i) != rhs.event_actions().Get(i))
			return false;
	}
	return true;
}

TEST(Config, ConfigNotAvailable) {
	ConfigContainer container("test_files/non-existing-file.conf");
	ConfigContainer container2{};
	EXPECT_TRUE(container.m_config == container2.m_config);
}

TEST(Config, ReadEmptyConfig) {
	ConfigContainer container("test_files/empty_file.conf");
	ConfigContainer container2{};
	EXPECT_TRUE(container.m_config == container2.m_config);
}

TEST(Config, WriteReadConfigNoAction) {
	std::experimental::filesystem::path temp_file{"test_files/temp_file"};
	ConfigContainer container{temp_file};
	EXPECT_NO_THROW(container.update_config());
	EXPECT_TRUE(std::experimental::filesystem::exists(temp_file));
	ConfigContainer container2{temp_file};
	EXPECT_TRUE(container.m_config == container2.m_config);
	container.m_config.set_zmq_uri("test");
	EXPECT_FALSE(container.m_config == container2.m_config);
	std::experimental::filesystem::remove(temp_file);
}

TEST(Config, WriteConfigNewDirectory) {
	std::experimental::filesystem::path temp_file = "test_files_temp_dir/test_file";
	ConfigContainer container{temp_file};
	EXPECT_NO_THROW(container.update_config());
	EXPECT_TRUE(std::experimental::filesystem::exists(temp_file));
	std::experimental::filesystem::remove(temp_file);
}

TEST(Config, SanitizeWorkingDirectory) {
	ConfigContainer config{};
	config.m_config.set_working_directory("test");
	config.sanitize_config_working_directory();
	EXPECT_EQ(config.m_config.working_directory(), "test/");
	config.m_config.set_working_directory("test/");
	config.sanitize_config_working_directory();
	EXPECT_EQ(config.m_config.working_directory(), "test/");
	config.m_config.set_working_directory("test//");
	config.sanitize_config_working_directory();
	EXPECT_EQ(config.m_config.working_directory(), "test/");
	config.m_config.set_working_directory("test/test2");
	config.sanitize_config_working_directory();
	EXPECT_EQ(config.m_config.working_directory(), "test/test2/");
	config.m_config.set_working_directory("test/test2/");
	config.sanitize_config_working_directory();
	EXPECT_EQ(config.m_config.working_directory(), "test/test2/");
	config.m_config.set_working_directory("test/test2//");
	config.sanitize_config_working_directory();
	EXPECT_EQ(config.m_config.working_directory(), "test/test2/");

}

TEST(Config, Subdirectories) {
	std::vector<std::string> f = get_available_subdirectories("test_files");
	EXPECT_EQ(2, f.size());
}
