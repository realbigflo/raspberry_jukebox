#include "raspberry_jukebox/handler.h"
#include "rpi_jukebox_config.pb.h"
#include "rpi_jukebox_events.pb.h"

#include <gtest/gtest.h>
#include <experimental/filesystem>

using namespace rpi_jukebox_events;
using namespace rpi_jukebox_config;
using namespace raspberry_jukebox;
using namespace raspberry_jukebox::config;
using namespace std::experimental::filesystem;


class HandlerRegister : public ::testing::Test {
protected:

	void set_long_path() {
		auto p = current_path();
		p += "/test_files/music/";
		m_config.m_config.set_working_directory(p);
		m_config.sanitize_config_working_directory();
	}

	void SetUp() override {
		m_temp_file = "test_files_temp_dir/test_file";
		m_config = ConfigContainer(m_temp_file);
		auto p = current_path();
		p += "/test_files/music/test_folder2";
		m_config.m_config.set_working_directory(p);
		m_config.sanitize_config_working_directory();
		m_rpc.set_id(RPC_ID::RPC_REGISTER);
	}

	void TearDown() override {
		try {
			std::experimental::filesystem::remove(m_temp_file);
		}catch(std::exception&) {
		}
	}

	ConfigContainer m_config;
	Rpc m_rpc;
	path m_temp_file;
};

TEST_F(HandlerRegister, WrongRPCNoId) {
	m_rpc.clear_id();
	EXPECT_THROW(handle_register(m_rpc, m_config), std::invalid_argument);
	EXPECT_FALSE(std::experimental::filesystem::exists(m_temp_file));
}

TEST_F(HandlerRegister, WrongRPCWrongId) {
	m_rpc.set_id(RPC_ID::RPC_EXECUTE);
	EXPECT_THROW(handle_register(m_rpc, m_config), std::invalid_argument) ;
	EXPECT_FALSE(std::experimental::filesystem::exists(m_temp_file));
}

TEST_F(HandlerRegister, WrongRPCNoActionNoID) {
	m_rpc.mutable_event_action()->set_filename("playlist.mp3");
	EXPECT_THROW(handle_register(m_rpc, m_config), std::invalid_argument);
	EXPECT_FALSE(std::experimental::filesystem::exists(m_temp_file));
}

TEST_F(HandlerRegister, WrongRPCNoUID) {
	m_rpc.mutable_event_action()->set_action(ACTION::ACTION_PLAY);
	m_rpc.mutable_event_action()->set_filename("playlist.mp3");
	EXPECT_THROW(handle_register(m_rpc, m_config), std::invalid_argument);
	EXPECT_FALSE(std::experimental::filesystem::exists(m_temp_file));
}

TEST_F(HandlerRegister, WrongRPCNoAction) {
	m_rpc.mutable_event_action()->mutable_event()->set_rfid_uid(0x1234);
	m_rpc.mutable_event_action()->set_filename("playlist.mp3");
	EXPECT_THROW(handle_register(m_rpc, m_config), std::invalid_argument);
	EXPECT_FALSE(std::experimental::filesystem::exists(m_temp_file));
}

TEST_F(HandlerRegister, SingleEventActionSTOP) {
	EventAction ea{};
	ea.set_action(ACTION::ACTION_STOP);
	ea.mutable_event()->set_uid("TestUID");
	EXPECT_NE(ea.event().uid(), std::string());
	m_rpc.mutable_event_action()->CopyFrom(ea);
	auto result = handle_register(m_rpc, m_config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(1, m_config.m_config.event_actions().size());
	EXPECT_EQ(m_config.m_config.event_actions(0).action(), ACTION::ACTION_STOP);
	EXPECT_TRUE(m_config.m_config.event_actions(0).has_event());
	EXPECT_EQ(m_config.m_config.event_actions(0).event().uid(), "TestUID");
	EXPECT_TRUE(std::experimental::filesystem::exists(m_temp_file));
}

TEST_F(HandlerRegister, SingleEventActionUID) {
	EventAction ea{};
	ea.set_action(ACTION::ACTION_PLAY);
	ea.mutable_event()->set_uid("TestUID");
	EXPECT_NE(ea.event().uid(), std::string());
	ea.set_filename("playlist.mp3");
	m_rpc.mutable_event_action()->CopyFrom(ea);
	auto result = handle_register(m_rpc, m_config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(1, m_config.m_config.event_actions().size());
	EXPECT_EQ(m_config.m_config.event_actions(0).action(), ACTION::ACTION_PLAY);
	EXPECT_TRUE(m_config.m_config.event_actions(0).has_event());
	EXPECT_EQ(m_config.m_config.event_actions(0).event().uid(), "TestUID");
	EXPECT_EQ(m_config.m_config.event_actions(0).filename(), "playlist.mp3");
	EXPECT_TRUE(std::experimental::filesystem::exists(m_temp_file));
}

TEST_F(HandlerRegister, SingleEventActionRFIDUID) {
	EventAction ea{};
	ea.set_action(ACTION::ACTION_PLAY);
	ea.mutable_event()->set_rfid_uid(0x1234);
	EXPECT_NE(ea.event().rfid_uid(), 0);
	ea.set_filename("playlist.mp3");
	m_rpc.mutable_event_action()->CopyFrom(ea);
	auto result = handle_register(m_rpc, m_config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(1, m_config.m_config.event_actions().size());
	EXPECT_EQ(m_config.m_config.event_actions(0).action(), ACTION::ACTION_PLAY);
	EXPECT_TRUE(m_config.m_config.event_actions(0).has_event());
	EXPECT_EQ(m_config.m_config.event_actions(0).event().rfid_uid(), 0x1234);
	EXPECT_EQ(m_config.m_config.event_actions(0).filename(), "playlist.mp3");
	EXPECT_TRUE(std::experimental::filesystem::exists(m_temp_file));
}

TEST_F(HandlerRegister, UpdateSingleEventAction) {
	set_long_path();
	EventAction ea{};
	ea.set_action(ACTION::ACTION_PLAY);
	ea.mutable_event()->set_rfid_uid(0x1234);
	ea.mutable_event()->set_uid("TestUID");
	EXPECT_NE(ea.event().uid(), std::string());
	ea.set_filename("test_folder2/playlist.mp3");
	m_rpc.mutable_event_action()->CopyFrom(ea);
	auto result = handle_register(m_rpc, m_config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(1, m_config.m_config.event_actions().size());
	EXPECT_EQ(m_config.m_config.event_actions(0).action(), ACTION::ACTION_PLAY);
	EXPECT_TRUE(m_config.m_config.event_actions(0).has_event());
	EXPECT_EQ(m_config.m_config.event_actions(0).event().uid(), "TestUID");
	EXPECT_EQ(m_config.m_config.event_actions(0).event().rfid_uid(), 0x1234);
	EXPECT_EQ(m_config.m_config.event_actions(0).filename(), "test_folder2/playlist.mp3");
	EXPECT_TRUE(std::experimental::filesystem::exists(m_temp_file));

	ea.set_filename("test_folder/music_file.mp3");
	m_rpc.mutable_event_action()->CopyFrom(ea);
	result = handle_register(m_rpc, m_config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(1, m_config.m_config.event_actions().size());
	EXPECT_EQ(m_config.m_config.event_actions(0).action(), ACTION::ACTION_PLAY);
	EXPECT_TRUE(m_config.m_config.event_actions(0).has_event());
	EXPECT_EQ(m_config.m_config.event_actions(0).event().uid(), "TestUID");
	EXPECT_EQ(m_config.m_config.event_actions(0).event().rfid_uid(), 0x1234);
	EXPECT_EQ(m_config.m_config.event_actions(0).filename(), "test_folder/music_file.mp3");
	EXPECT_TRUE(std::experimental::filesystem::exists(m_temp_file));
}

TEST_F(HandlerRegister, UpdateSingleEventActionUID) {
	set_long_path();
	EventAction ea{};
	ea.set_action(ACTION::ACTION_PLAY);
	ea.mutable_event()->set_uid("TestUID");
	EXPECT_NE(ea.event().uid(), std::string());
	ea.set_filename("test_folder2/playlist.mp3");
	m_rpc.mutable_event_action()->CopyFrom(ea);
	auto result = handle_register(m_rpc, m_config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(1, m_config.m_config.event_actions().size());
	EXPECT_EQ(m_config.m_config.event_actions(0).action(), ACTION::ACTION_PLAY);
	EXPECT_TRUE(m_config.m_config.event_actions(0).has_event());
	EXPECT_EQ(m_config.m_config.event_actions(0).event().uid(), "TestUID");
	EXPECT_EQ(m_config.m_config.event_actions(0).filename(), "test_folder2/playlist.mp3");
	EXPECT_TRUE(std::experimental::filesystem::exists(m_temp_file));

	ea.set_filename("test_folder/music_file.mp3");
	m_rpc.mutable_event_action()->CopyFrom(ea);
	result = handle_register(m_rpc, m_config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(1, m_config.m_config.event_actions().size());
	EXPECT_EQ(m_config.m_config.event_actions(0).action(), ACTION::ACTION_PLAY);
	EXPECT_TRUE(m_config.m_config.event_actions(0).has_event());
	EXPECT_EQ(m_config.m_config.event_actions(0).event().uid(), "TestUID");
	EXPECT_EQ(m_config.m_config.event_actions(0).filename(), "test_folder/music_file.mp3");
	EXPECT_TRUE(std::experimental::filesystem::exists(m_temp_file));
}

TEST_F(HandlerRegister, UpdateSingleEventActionRFID_UID) {
	set_long_path();
	EventAction ea{};
	ea.set_action(ACTION::ACTION_PLAY);
	ea.mutable_event()->set_rfid_uid(0x1234);
	ea.set_filename("test_folder2/playlist.mp3");
	m_rpc.mutable_event_action()->CopyFrom(ea);
	auto result = handle_register(m_rpc, m_config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(1, m_config.m_config.event_actions().size());
	EXPECT_EQ(m_config.m_config.event_actions(0).action(), ACTION::ACTION_PLAY);
	EXPECT_TRUE(m_config.m_config.event_actions(0).has_event());
	EXPECT_EQ(m_config.m_config.event_actions(0).event().rfid_uid(), 0x1234);
	EXPECT_EQ(m_config.m_config.event_actions(0).filename(), "test_folder2/playlist.mp3");
	EXPECT_TRUE(std::experimental::filesystem::exists(m_temp_file));

	ea.set_filename("test_folder/music_file.mp3");
	m_rpc.mutable_event_action()->CopyFrom(ea);
	result = handle_register(m_rpc, m_config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(1, m_config.m_config.event_actions().size());
	EXPECT_EQ(m_config.m_config.event_actions(0).action(), ACTION::ACTION_PLAY);
	EXPECT_TRUE(m_config.m_config.event_actions(0).has_event());
	EXPECT_EQ(m_config.m_config.event_actions(0).event().rfid_uid(), 0x1234);
	EXPECT_EQ(m_config.m_config.event_actions(0).filename(), "test_folder/music_file.mp3");
	EXPECT_TRUE(std::experimental::filesystem::exists(m_temp_file));
}

TEST_F(HandlerRegister, MultipleEventAction) {
	set_long_path();
	EventAction ea{};
	ea.set_action(ACTION::ACTION_PLAY);
	ea.mutable_event()->set_rfid_uid(0x1234);
	ea.mutable_event()->set_uid("TestUID");
	EXPECT_NE(ea.event().uid(), std::string());
	EXPECT_NE(ea.event().rfid_uid(), 0);
	ea.set_filename("test_folder2/playlist.mp3");
	m_config.m_config.add_event_actions()->CopyFrom(ea);

	ea.set_action(ACTION::ACTION_PLAY);
	ea.mutable_event()->set_rfid_uid(0x4321);
	ea.mutable_event()->set_uid("TestUID2");
	EXPECT_NE(ea.event().uid(), std::string());
	EXPECT_NE(ea.event().rfid_uid(), 0);
	ea.set_filename("test_folder/music_file.mp3");
	m_rpc.mutable_event_action()->CopyFrom(ea);

	auto result = handle_register(m_rpc, m_config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(2, m_config.m_config.event_actions().size());
	EXPECT_EQ(m_config.m_config.event_actions(1).action(), ACTION::ACTION_PLAY);
	EXPECT_TRUE(m_config.m_config.event_actions(1).has_event());
	EXPECT_EQ(m_config.m_config.event_actions(1).event().uid(), "TestUID2");
	EXPECT_EQ(m_config.m_config.event_actions(1).event().rfid_uid(), 0x4321);
	EXPECT_EQ(m_config.m_config.event_actions(1).filename(), "test_folder/music_file.mp3");
	EXPECT_TRUE(std::experimental::filesystem::exists(m_temp_file));
}

