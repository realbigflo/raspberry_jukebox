#include "raspberry_jukebox/handler.h"
#include "rpi_jukebox_config.pb.h"
#include "rpi_jukebox_events.pb.h"

#include <gtest/gtest.h>
#include <algorithm>
#include <iterator>
#include <experimental/filesystem>

using namespace rpi_jukebox_events;
using namespace rpi_jukebox_config;
using namespace raspberry_jukebox;
using namespace raspberry_jukebox::config;
using namespace std::experimental::filesystem;

TEST(Handler, WrongRPC) {
	ConfigContainer config{};
	Rpc rpc;
	EXPECT_THROW(handle_query_available(rpc, config), std::exception);
	rpc.set_id(RPC_ID::RPC_EXECUTE);
	EXPECT_THROW(handle_query_available(rpc, config), std::exception);
}

TEST(AvailableFiles, NoFileAvailable) {
	ConfigContainer config{};
	auto p = current_path();
	p += "/test_files/no_directory";
	config.m_config.set_working_directory(p);
	config.sanitize_config_working_directory();
	Rpc rpc;
	rpc.set_id(RPC_ID::RPC_QUERY_AVAILABLE);
	auto result = handle_query_available(rpc, config);
	EXPECT_EQ(RESULT::RES_OK, result->result());
	EXPECT_EQ(0, result->available_files().size());
}

TEST(AvailableFiles, UnrecognizedFile) {
	ConfigContainer config{};
	auto p = current_path();
	p += "/test_files/music/test_folder3";
	config.m_config.set_working_directory(p);
	config.sanitize_config_working_directory();
	Rpc rpc;
	rpc.set_id(RPC_ID::RPC_QUERY_AVAILABLE);
	auto result = handle_query_available(rpc, config);
	EXPECT_EQ(RESULT::RES_OK, result->result());
	EXPECT_EQ(0, result->available_files().size());
}

TEST(AvailableFiles, SingleFile) {
	ConfigContainer config{};
	auto p = current_path();
	p += "/test_files/music/test_folder2";
	config.m_config.set_working_directory(p);
	config.sanitize_config_working_directory();
	Rpc rpc;
	rpc.set_id(RPC_ID::RPC_QUERY_AVAILABLE_FILES);
	auto result = handle_query_available_files(rpc, config);
	EXPECT_EQ(RESULT::RES_OK, result->result());
	EXPECT_EQ(1, result->available_files().size());
	EXPECT_EQ(result->available_files(0), "playlist.mp3");
}

TEST(AvailableFiles, SingleFileWDWithSlash) {
	ConfigContainer config{};
	auto p = current_path();
	p += "/test_files/music/test_folder2/";
	config.m_config.set_working_directory(p);
	config.sanitize_config_working_directory();
	Rpc rpc;
	rpc.set_id(RPC_ID::RPC_QUERY_AVAILABLE_FILES);
	auto result = handle_query_available_files(rpc, config);
	EXPECT_EQ(RESULT::RES_OK, result->result());
	EXPECT_EQ(1, result->available_files().size());
	EXPECT_EQ(result->available_files(0), "playlist.mp3");
}

TEST(AvailableFiles, MultipleDirectories) {
	ConfigContainer config { };
	auto p = current_path();
	p += "/test_files/music";
	config.m_config.set_working_directory(p);
	config.sanitize_config_working_directory();
	Rpc rpc;
	rpc.set_id(RPC_ID::RPC_QUERY_AVAILABLE);
	auto result = handle_query_available(rpc, config);
	EXPECT_EQ(RESULT::RES_OK, result->result());
	EXPECT_EQ(2, result->available_files().size());
	EXPECT_NE(std::find(std::cbegin(result->available_files()), std::cend(result->available_files()), "test_folder"),
			result->available_files().cend());
	EXPECT_NE(std::find(std::cbegin(result->available_files()), std::cend(result->available_files()), "test_folder2"),
			result->available_files().cend());
}


TEST(AvailableFiles, MultipleFilesAvailable) {
	ConfigContainer config{};
	auto p = current_path();
	p += "/test_files/music";
	config.m_config.set_working_directory(p);
	config.sanitize_config_working_directory();
	Rpc rpc;
	rpc.set_id(RPC_ID::RPC_QUERY_AVAILABLE_FILES);
	auto result = handle_query_available_files(rpc, config);
	EXPECT_EQ(RESULT::RES_OK, result->result());
	EXPECT_EQ(3, result->available_files().size());
	EXPECT_NE(std::find(std::cbegin(result->available_files()), std::cend(result->available_files()), "test_folder/music_file.mp3"), result->available_files().cend());
	EXPECT_NE(std::find(std::cbegin(result->available_files()), std::cend(result->available_files()), "test_folder/music_file.ogg"), result->available_files().cend());
	EXPECT_NE(
			std::find(std::cbegin(result->available_files()), std::cend(result->available_files()),
					"test_folder2/playlist.mp3"), result->available_files().cend());
}
