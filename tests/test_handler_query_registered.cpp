#include "raspberry_jukebox/handler.h"
#include "rpi_jukebox_config.pb.h"
#include "rpi_jukebox_events.pb.h"

#include <gtest/gtest.h>
#include <experimental/filesystem>

using namespace rpi_jukebox_events;
using namespace rpi_jukebox_config;
using namespace raspberry_jukebox;
using namespace raspberry_jukebox::config;

TEST(Handler, WrongRPC) {
	ConfigContainer config{};
	Rpc rpc{};
	EXPECT_THROW(handle_query_registered(rpc, config), std::exception);
	rpc.set_id(RPC_ID::RPC_EXECUTE);
	EXPECT_THROW(handle_query_registered(rpc, config), std::exception);
}

TEST(RegisteredActions, EmptyConfig) {
	ConfigContainer config{};
	Rpc rpc{};
	rpc.set_id(RPC_ID::RPC_QUERY_REGISTERED);
	auto result = handle_query_registered(rpc, config);
	EXPECT_EQ(RESULT::RES_OK, result->result());
}

TEST(RegisteredActions, SingleEventAction) {
	ConfigContainer config{};
	Rpc rpc{};
	rpc.set_id(RPC_ID::RPC_QUERY_REGISTERED);
	EventAction ea{};
	ea.set_action(ACTION::ACTION_PLAY);
	ea.mutable_event()->set_rfid_uid(0x1234);
	ea.mutable_event()->set_uid("TestUID");
	EXPECT_NE(ea.event().uid(), std::string());
	EXPECT_NE(ea.event().rfid_uid(), 0);
	ea.set_filename("test_file");
	ea.set_working_directory("tests");
	config.m_config.add_event_actions()->CopyFrom(ea);
	auto result = handle_query_registered(rpc, config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(1, result->event_actions().size());
	EXPECT_EQ(result->event_actions(0).action(), ACTION::ACTION_PLAY);
	EXPECT_TRUE(result->event_actions(0).has_event());
	EXPECT_EQ(result->event_actions(0).event().uid(), "TestUID");
	EXPECT_EQ(result->event_actions(0).event().rfid_uid(), 0x1234);
	EXPECT_EQ(result->event_actions(0).filename(), "test_file");
	EXPECT_EQ(result->event_actions(0).working_directory(), "tests");

}

TEST(RegisteredActions, SingleEventActionUID) {
	ConfigContainer config{};
	Rpc rpc{};
	rpc.set_id(RPC_ID::RPC_QUERY_REGISTERED);
	EventAction ea{};
	ea.set_action(ACTION::ACTION_PLAY);
	ea.mutable_event()->set_uid("TestUID");
	EXPECT_NE(ea.event().uid(), std::string());
	ea.set_filename("test_file");
	ea.set_working_directory("tests");
	config.m_config.add_event_actions()->CopyFrom(ea);
	auto result = handle_query_registered(rpc, config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(1, result->event_actions().size());
	EXPECT_EQ(result->event_actions(0).action(), ACTION::ACTION_PLAY);
	EXPECT_TRUE(result->event_actions(0).has_event());
	EXPECT_EQ(result->event_actions(0).event().uid(), "TestUID");
	EXPECT_EQ(result->event_actions(0).event().rfid_uid(), 0);
	EXPECT_EQ(result->event_actions(0).filename(), "test_file");
	EXPECT_EQ(result->event_actions(0).working_directory(), "tests");

}
TEST(RegisteredActions, SingleEventActionRFIDUID) {
	ConfigContainer config{};
	Rpc rpc{};
	rpc.set_id(RPC_ID::RPC_QUERY_REGISTERED);
	EventAction ea{};
	ea.set_action(ACTION::ACTION_PLAY);
	ea.mutable_event()->set_rfid_uid(0x1234);
	EXPECT_EQ(ea.event().uid(), std::string());
	EXPECT_NE(ea.event().rfid_uid(), 0);
	ea.set_filename("test_file");
	ea.set_working_directory("tests");
	config.m_config.add_event_actions()->CopyFrom(ea);
	auto result = handle_query_registered(rpc, config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(1, result->event_actions().size());
	EXPECT_EQ(result->event_actions(0).action(), ACTION::ACTION_PLAY);
	EXPECT_TRUE(result->event_actions(0).has_event());
	EXPECT_EQ(result->event_actions(0).event().uid(), "");
	EXPECT_EQ(result->event_actions(0).event().rfid_uid(), 0x1234);
	EXPECT_EQ(result->event_actions(0).filename(), "test_file");
	EXPECT_EQ(result->event_actions(0).working_directory(), "tests");

}

TEST(RegisteredActions, MultipleEventAction) {
	ConfigContainer config{};
	Rpc rpc{};
	rpc.set_id(RPC_ID::RPC_QUERY_REGISTERED);
	EventAction ea{};
	config.m_config.add_event_actions()->CopyFrom(ea);
	ea.set_action(ACTION::ACTION_PLAY);
	ea.mutable_event()->set_rfid_uid(0x4321);
	ea.mutable_event()->set_uid("TestUID");
	EXPECT_NE(ea.event().uid(), std::string());
	EXPECT_NE(ea.event().rfid_uid(), 0);
	ea.set_filename("test_file2");
	ea.set_working_directory("tests2");
	config.m_config.add_event_actions()->CopyFrom(ea);
	auto result = handle_query_registered(rpc, config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
	EXPECT_EQ(2, result->event_actions().size());
	EXPECT_EQ(result->event_actions(1).action(), ACTION::ACTION_PLAY);
	EXPECT_TRUE(result->event_actions(1).has_event());
	EXPECT_EQ(result->event_actions(1).event().uid(), "TestUID");
	EXPECT_EQ(result->event_actions(1).event().rfid_uid(), 0x4321);
	EXPECT_EQ(result->event_actions(1).filename(), "test_file2");
	EXPECT_EQ(result->event_actions(1).working_directory(), "tests2");
}
