// cppcheck-suppress missingInclude
#include "rpi_jukebox_events.pb.h"
#include "raspberry_jukebox/provider.h"
#include "raspberry_jukebox/provider_errors.h"

#include <gtest/gtest.h>
#include <iostream>
#include <thread>

using namespace raspberry_jukebox;

class TestEventReader: public EventReader {
public:
	TestEventReader() :
			m_counter(0) {
	}
	virtual ~TestEventReader() {
	}

	/// Blocking call to return next event from the source
	///
	/// Can throw a StopException if the reader is going to quit reading.
	std::unique_ptr<rpi_jukebox_events::Event> get_next_event() override {
		auto event = std::make_unique<rpi_jukebox_events::Event>();
		event->set_uid(std::to_string(this->m_counter++));
		return event;
	}

	void signal_stop() override {
	}

private:
	int m_counter;
};

class ProviderTestFixture: public ::testing::Test {
protected:
	virtual void SetUp() {
		this->m_provider = std::make_unique<RpcSender>(DEFAULT_URI_CLIENT);
		this->m_sock = std::make_unique<zmq::socket_t>(m_context, ZMQ_REP);
		this->m_sock->bind(DEFAULT_URI_SERVER);
	}

	zmq::context_t m_context;
	std::unique_ptr<zmq::socket_t> m_sock;
	std::unique_ptr<RpcSender> m_provider;
};

TEST_F(ProviderTestFixture, StartProvider) {
	EXPECT_EQ(this->m_provider->to_str(), DEFAULT_URI_CLIENT);
}

TEST_F(ProviderTestFixture, SimpleProvider) {

	auto reader = std::make_unique<TestEventReader>();
	{
		std::thread t([&] {
			zmq::message_t zmq_msg;
			this->m_sock->recv(&zmq_msg);
			std::string msg_str(static_cast<char*>(zmq_msg.data()), zmq_msg.size());
			rpi_jukebox_events::Rpc rpc;
			EXPECT_TRUE(rpc.ParseFromString(msg_str));
			EXPECT_EQ(rpc.event().uid(), "0");
			EXPECT_EQ(rpc.id(), rpi_jukebox_events::RPC_ID::RPC_EXECUTE);

			rpi_jukebox_events::RpcResult resp_result;
			resp_result.set_result(rpi_jukebox_events::RES_OK);
			std::string resp_msg_str;
			ASSERT_TRUE(resp_result.SerializeToString(&resp_msg_str));
			zmq::message_t response(resp_msg_str.size());
			std::memcpy((void *) response.data(), resp_msg_str.c_str(), resp_msg_str.size());
			this->m_sock->send(response);
		});

		auto event = reader->get_next_event();
		EXPECT_NE(event->uid(), std::string());
		std::unique_ptr<rpi_jukebox_events::RpcResult> result = nullptr;
		EXPECT_NO_THROW(result = this->m_provider->rpc(rpi_jukebox_events::RPC_ID::RPC_EXECUTE, std::move(event)));
		EXPECT_EQ(result->result(), rpi_jukebox_events::RES_OK);
		t.join();
	}

	{
		std::thread t([&] {
			zmq::message_t zmq_msg;
			this->m_sock->recv(&zmq_msg);
			std::string msg_str(static_cast<char*>(zmq_msg.data()), zmq_msg.size());
			rpi_jukebox_events::Rpc rpc;
			EXPECT_TRUE(rpc.ParseFromString(msg_str));
			EXPECT_EQ(rpc.event().uid(), "1");
			EXPECT_EQ(rpc.id(), rpi_jukebox_events::RPC_ID::RPC_EXECUTE);

			rpi_jukebox_events::RpcResult resp_result;
			resp_result.set_result(rpi_jukebox_events::RES_OK);
			std::string resp_msg_str;
			ASSERT_TRUE(resp_result.SerializeToString(&resp_msg_str));
			zmq::message_t response(resp_msg_str.size());
			std::memcpy((void *) response.data(), resp_msg_str.c_str(), resp_msg_str.size());
			this->m_sock->send(response);
		});

		auto event = reader->get_next_event();
		EXPECT_NE(event->uid(), std::string());
		std::unique_ptr<rpi_jukebox_events::RpcResult> result = nullptr;
		EXPECT_NO_THROW(result = this->m_provider->rpc(rpi_jukebox_events::RPC_ID::RPC_EXECUTE, std::move(event)));
		EXPECT_EQ(result->result(), rpi_jukebox_events::RES_OK);
		t.join();
	}
}

TEST_F(ProviderTestFixture, RpcOnly) {
	std::thread t([&] {
		zmq::message_t zmq_msg;
		this->m_sock->recv(&zmq_msg);
		std::string msg_str(static_cast<char*>(zmq_msg.data()), zmq_msg.size());
		rpi_jukebox_events::Rpc rpc;
		EXPECT_TRUE(rpc.ParseFromString(msg_str));
		EXPECT_FALSE(rpc.has_event());
		EXPECT_FALSE(rpc.has_event_action());
		EXPECT_EQ(rpc.id(), rpi_jukebox_events::RPC_ID::RPC_QUERY_REGISTERED);

		rpi_jukebox_events::RpcResult resp_result;
		resp_result.set_result(rpi_jukebox_events::RES_OK);
		std::string resp_msg_str;
		ASSERT_TRUE(resp_result.SerializeToString(&resp_msg_str));
		zmq::message_t response(resp_msg_str.size());
		std::memcpy((void *) response.data(), resp_msg_str.c_str(), resp_msg_str.size());
		this->m_sock->send(response);
	});

	std::unique_ptr<rpi_jukebox_events::RpcResult> result = nullptr;
	EXPECT_NO_THROW(result = this->m_provider->rpc(rpi_jukebox_events::RPC_ID::RPC_QUERY_REGISTERED));
	EXPECT_EQ(result->result(), rpi_jukebox_events::RES_OK);
	t.join();

}

TEST_F(ProviderTestFixture, MalformedResponse) {

	std::thread t([&]{ zmq::message_t zmq_msg;
		this->m_sock->recv(&zmq_msg);

		std::string msg_str("Malformed");
		zmq::message_t request(msg_str.size());
		std::memcpy((void *) request.data(), msg_str.c_str(), msg_str.size());
		this->m_sock->send(request);
	});

	auto reader = std::make_unique<TestEventReader>();
	auto event = reader->get_next_event();
	EXPECT_NE(event->uid(), std::string());
	EXPECT_THROW(this->m_provider->rpc(rpi_jukebox_events::RPC_ID::RPC_EXECUTE, std::move(event)), MessageMalformedError);
	t.join();
}
