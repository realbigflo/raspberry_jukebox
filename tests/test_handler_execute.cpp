#include "raspberry_jukebox/handler.h"
#include "rpi_jukebox_config.pb.h"
#include "rpi_jukebox_events.pb.h"

#include <gtest/gtest.h>
#include <experimental/filesystem>

using namespace rpi_jukebox_events;
using namespace rpi_jukebox_config;
using namespace raspberry_jukebox;
using namespace raspberry_jukebox::config;

static bool g_STOP_CALLED = false, g_PAUSE_CALLED = false, g_PLAY_CALLED = false;

std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::ExecuteHandler::handle_action_play(const rpi_jukebox_events::Rpc& , raspberry_jukebox::config::ConfigContainer&) {
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	result->set_result(rpi_jukebox_events::RESULT::RES_OK);
	g_PLAY_CALLED = true;
	return result;
}
std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::ExecuteHandler::handle_action_pause() {
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	result->set_result(rpi_jukebox_events::RESULT::RES_OK);
	g_PAUSE_CALLED = true;
	return result;
}
std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::ExecuteHandler::handle_action_stop() {
	auto result = std::make_unique<rpi_jukebox_events::RpcResult>();
	result->set_result(rpi_jukebox_events::RESULT::RES_OK);
	g_STOP_CALLED = true;
	return result;
}
std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::ExecuteHandler::handle_action_forward() {
	return nullptr;
}
std::unique_ptr<rpi_jukebox_events::RpcResult> raspberry_jukebox::ExecuteHandler::handle_action_rewind() {
	return nullptr;
}


TEST(Execute, WrongRPC) {
	ConfigContainer config{};
	Rpc rpc{};
	raspberry_jukebox::ExecuteHandler handler{};
	EXPECT_THROW(handler.handle_execute(rpc, config), std::exception);
	rpc.set_id(RPC_ID::RPC_DELETE);
	EXPECT_THROW(handler.handle_execute(rpc, config), std::exception);
}

TEST(Execute, EmptyConfig) {
	ConfigContainer config{};
	Rpc rpc{};
	rpc.set_id(RPC_ID::RPC_EXECUTE);
	raspberry_jukebox::ExecuteHandler handler{};
	auto result = handler.handle_execute(rpc, config);
	EXPECT_EQ(RESULT::RES_ERROR, result->result());
}

TEST(Execute, SingleEventAction) {
	ConfigContainer config{};
	Rpc rpc{};
	raspberry_jukebox::ExecuteHandler handler{};
	rpc.set_id(RPC_ID::RPC_EXECUTE);
	EventAction ea{};
	ea.set_action(ACTION::ACTION_PLAY);
	ea.mutable_event()->set_rfid_uid(0x1234);
	ea.mutable_event()->set_uid("TestUID");
	EXPECT_NE(ea.event().uid(), std::string());
	EXPECT_NE(ea.event().rfid_uid(), 0);
	ea.set_filename("test_file");
	ea.set_working_directory("tests");
	config.m_config.add_event_actions()->CopyFrom(ea);
	rpc.mutable_event()->set_rfid_uid(0x1234);
	rpc.mutable_event()->set_uid("TestUID");
	auto result = handler.handle_execute(rpc, config);
	EXPECT_EQ(result->result(), RESULT::RES_OK);
}

TEST(Execute, MultipleEventAction) {
	ConfigContainer config{};
	raspberry_jukebox::ExecuteHandler handler{};
	{
		EventAction ea{};
		ea.set_action(ACTION::ACTION_STOP);
		ea.mutable_event()->set_uid("TestSTOP");
		config.m_config.add_event_actions()->CopyFrom(ea);
	}

	{
		EventAction ea{};
		ea.set_action(ACTION::ACTION_PAUSE);
		ea.mutable_event()->set_uid("TestPAUSE");
		config.m_config.add_event_actions()->CopyFrom(ea);
	}

	{
		EventAction ea{};
		ea.set_action(ACTION::ACTION_PLAY);
		ea.mutable_event()->set_rfid_uid(0x1234);
		config.m_config.add_event_actions()->CopyFrom(ea);
	}

	{
		Rpc rpc{};
		rpc.set_id(RPC_ID::RPC_EXECUTE);
		rpc.mutable_event()->set_uid("TestSTOP");
		g_STOP_CALLED = g_PAUSE_CALLED = g_PLAY_CALLED = false;
		auto result = handler.handle_execute(rpc, config);
		EXPECT_EQ(result->result(), RESULT::RES_OK);
		EXPECT_TRUE(g_STOP_CALLED);
		EXPECT_FALSE(g_PAUSE_CALLED);
		EXPECT_FALSE(g_PLAY_CALLED);
	}
	{
		Rpc rpc{};
		rpc.set_id(RPC_ID::RPC_EXECUTE);
		rpc.mutable_event()->set_uid("TestPAUSE");
		g_STOP_CALLED = g_PAUSE_CALLED = g_PLAY_CALLED = false;
		auto result = handler.handle_execute(rpc, config);
		EXPECT_EQ(result->result(), RESULT::RES_OK);
		EXPECT_FALSE(g_STOP_CALLED);
		EXPECT_TRUE(g_PAUSE_CALLED);
		EXPECT_FALSE(g_PLAY_CALLED);
	}
	{
		Rpc rpc{};
		rpc.set_id(RPC_ID::RPC_EXECUTE);
		rpc.mutable_event()->set_rfid_uid(0x1234);
		g_STOP_CALLED = g_PAUSE_CALLED = g_PLAY_CALLED = false;
		auto result = handler.handle_execute(rpc, config);
		EXPECT_EQ(result->result(), RESULT::RES_OK);
		EXPECT_FALSE(g_STOP_CALLED);
		EXPECT_FALSE(g_PAUSE_CALLED);
		EXPECT_TRUE(g_PLAY_CALLED);
	}
}
