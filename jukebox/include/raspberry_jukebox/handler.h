#pragma once

// cppcheck-suppress missingInclude
#include "rpi_jukebox_events.pb.h"
#include "raspberry_jukebox/config.h"

#include <vlc/vlc.h>

#include <unordered_map>

namespace raspberry_jukebox {

std::unique_ptr<rpi_jukebox_events::RpcResult> handle_query_registered(const rpi_jukebox_events::Rpc& rpc, raspberry_jukebox::config::ConfigContainer& config);
std::unique_ptr<rpi_jukebox_events::RpcResult> handle_query_available(const rpi_jukebox_events::Rpc& rpc, raspberry_jukebox::config::ConfigContainer& config);
std::unique_ptr<rpi_jukebox_events::RpcResult> handle_query_available_files(const rpi_jukebox_events::Rpc& rpc, raspberry_jukebox::config::ConfigContainer& config);
std::unique_ptr<rpi_jukebox_events::RpcResult> handle_register(const rpi_jukebox_events::Rpc& rpc, raspberry_jukebox::config::ConfigContainer& config);
std::unique_ptr<rpi_jukebox_events::RpcResult> handle_delete(const rpi_jukebox_events::Rpc& rpc, raspberry_jukebox::config::ConfigContainer& config);

class ExecuteHandler {
public:
	ExecuteHandler();
	ExecuteHandler(const raspberry_jukebox::config::ConfigContainer& config);
	std::unique_ptr<rpi_jukebox_events::RpcResult> handle_execute(const rpi_jukebox_events::Rpc& rpc, raspberry_jukebox::config::ConfigContainer& config);
	std::unique_ptr<rpi_jukebox_events::RpcResult> handle_action_play(const rpi_jukebox_events::Rpc& rpc, raspberry_jukebox::config::ConfigContainer& config);
	std::unique_ptr<rpi_jukebox_events::RpcResult> handle_action_pause();
	std::unique_ptr<rpi_jukebox_events::RpcResult> handle_action_stop();
	std::unique_ptr<rpi_jukebox_events::RpcResult> handle_action_forward();
	std::unique_ptr<rpi_jukebox_events::RpcResult> handle_action_rewind();
private:

	void stop_and_release_media_player();
	int play_media_player();
	void pause_media_player();
	void forward_media_player();
	void rewind_media_player();

	libvlc_instance_t * m_vlc_inst;
	libvlc_media_player_t *m_vlc_media_player;
	libvlc_media_list_player_t* m_vlc_media_list_player;
	std::unordered_map<std::string, libvlc_media_t*> m_vlc_media;
	std::unordered_map<std::string, libvlc_media_list_t*> m_vlc_media_list;
};

}
