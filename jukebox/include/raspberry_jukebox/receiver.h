#pragma once

#include "raspberry_jukebox/common.h"

// cppcheck-suppress missingInclude
#include "rpi_jukebox_events.pb.h"
// cppcheck-suppress missingInclude
#include "zmq.hpp"

#include <iostream>
#include <string>

namespace raspberry_jukebox {

class ZMQReceiver {
public:
	ZMQReceiver(const std::string& uri);
	~ZMQReceiver();
	std::unique_ptr<rpi_jukebox_events::Rpc> get_next_rpc();
	void send_rpc_execution_result(const rpi_jukebox_events::RpcResult& result);
	std::string to_str() const;
private:
	zmq::context_t m_zmq_context;
	zmq::socket_t m_zmq_socket;
	const std::string m_uri;
};

std::ostream& operator<<(std::ostream& ostream, const ZMQReceiver& receiver);

}
