#pragma once

#include "raspberry_jukebox/common_errors.h"

#include <exception>

namespace raspberry_jukebox {
namespace receiver {

class MissingSendresultError: public std::exception {
};

class MissingGetNextEventError: public std::exception {
};

}
}
