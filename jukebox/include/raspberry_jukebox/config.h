#pragma once

#include "rpi_jukebox_config.pb.h"
#include <experimental/filesystem>
#include <string>
#include <memory>

namespace raspberry_jukebox {
namespace config {

class ConfigContainer {
public:
	ConfigContainer();
	ConfigContainer(const std::experimental::filesystem::path& filename);
	void update_config();

	rpi_jukebox_config::Config m_config;
	void sanitize_config_working_directory();

private:
	std::experimental::filesystem::path m_path;

};
std::unique_ptr<std::vector<std::string>> get_available_files(const std::experimental::filesystem::path& working_directory);
std::vector<std::string> get_available_subdirectories(const std::experimental::filesystem::path& working_directory);
std::vector<std::string> get_available_subdirectories_and_files(const std::experimental::filesystem::path& working_directory);
}
}
