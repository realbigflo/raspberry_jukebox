#include "raspberry_jukebox/receiver.h"
#include "raspberry_jukebox/receiver_errors.h"

#include "zmq.hpp"

#include <iostream>
#include <string>
#include <thread>


raspberry_jukebox::ZMQReceiver::ZMQReceiver(const std::string& uri) : m_zmq_context(), m_zmq_socket(m_zmq_context, ZMQ_REP), m_uri(uri) {
	this->m_zmq_socket.bind(m_uri.c_str());
}

raspberry_jukebox::ZMQReceiver::~ZMQReceiver() {
	this->m_zmq_socket.close();
}

std::string raspberry_jukebox::ZMQReceiver::to_str() const {
	return this->m_uri;
}

std::unique_ptr<rpi_jukebox_events::Rpc> raspberry_jukebox::ZMQReceiver::get_next_rpc() {

	zmq::message_t zmq_msg;
	try {
		this->m_zmq_socket.recv(&zmq_msg);
	} catch (zmq::error_t& err) {
		if (err.num() == EFSM)
			throw receiver::MissingSendresultError();
		else
			throw err;  // all other errors are not handled
	}
	std::string msg_str(static_cast<char*>(zmq_msg.data()), zmq_msg.size());
	auto rpc = std::make_unique<rpi_jukebox_events::Rpc>();
	if (!rpc->ParseFromString(msg_str)) {
		throw MessageMalformedError();
	}
	return rpc;
}

void raspberry_jukebox::ZMQReceiver::send_rpc_execution_result(const rpi_jukebox_events::RpcResult& result) {
	std::string msg_str;
	result.SerializeToString(&msg_str);
	zmq::message_t response(msg_str.size());
	std::memcpy((void *) response.data(), msg_str.c_str(), msg_str.size());
	try {
		this->m_zmq_socket.send(response);
	} catch (zmq::error_t& err) {
		if (err.num() == EFSM)
			throw receiver::MissingGetNextEventError();
		else
			throw err;  // all other errors are not handled
	}
}

std::ostream& operator<<(std::ostream& ostream, const raspberry_jukebox::ZMQReceiver& receiver) {
	ostream << receiver.to_str();
	return ostream;
}
