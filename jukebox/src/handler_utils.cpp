#include "raspberry_jukebox/config.h"
#include "raspberry_jukebox_internals/handler_utils.h"
#include "rpi_jukebox_events.pb.h"

#include <algorithm>

void raspberry_jukebox::internals::check_rpc_id(rpi_jukebox_events::RPC_ID id,
		const rpi_jukebox_events::Rpc& rpc) {
	if (rpc.id() != id) {
		throw std::invalid_argument("Invalid call to this RPC");
	}
}

decltype(std::declval<rpi_jukebox_events::RpcResult>().event_actions().cbegin()) raspberry_jukebox::internals::find_event_action(
		decltype(std::declval<rpi_jukebox_events::RpcResult>().event_actions()) event_actions,
		const std::string& uid, uint64_t rfid_uid) {
	auto comp = [&](const rpi_jukebox_events::EventAction& ea) -> bool {
		return ea.has_event() &&
		ea.event().uid() == uid &&
		ea.event().rfid_uid() == rfid_uid;
	};
	return std::find_if(std::cbegin(event_actions), std::cend(event_actions),
			comp);
}

decltype(std::declval<rpi_jukebox_events::RpcResult>().mutable_event_actions()->begin()) raspberry_jukebox::internals::find_mutable_event_action(
		decltype(std::declval<rpi_jukebox_events::RpcResult>().mutable_event_actions()) event_actions,
		const std::string& uid, uint64_t rfid_uid) {
	auto comp = [&](const rpi_jukebox_events::EventAction& ea) -> bool {
		return ea.has_event() &&
		ea.event().uid() == uid &&
		ea.event().rfid_uid() == rfid_uid;
	};
	return std::find_if(event_actions->begin(), event_actions->end(), comp);
}
