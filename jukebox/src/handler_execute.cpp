#include "raspberry_jukebox/config.h"
#include "raspberry_jukebox/handler.h"
#include "raspberry_jukebox_internals/handler_utils.h"
#include "rpi_jukebox_events.pb.h"

#include <boost/log/trivial.hpp>
#include <vlc/vlc.h>

#include <algorithm>

using namespace raspberry_jukebox;


ExecuteHandler::ExecuteHandler() : m_vlc_inst(nullptr), m_vlc_media_player(nullptr), m_vlc_media_list_player(nullptr), m_vlc_media(), m_vlc_media_list() {}

ExecuteHandler::ExecuteHandler(const raspberry_jukebox::config::ConfigContainer& config) : m_vlc_media_player(nullptr), m_vlc_media_list_player(nullptr), m_vlc_media(), m_vlc_media_list(){
	this->m_vlc_inst = libvlc_new (0, nullptr);
	auto files = raspberry_jukebox::config::get_available_files(config.m_config.working_directory());
	for (const auto& f : *files) {
		std::string temp = config.m_config.working_directory() + f;
		BOOST_LOG_TRIVIAL(debug)<< "Add media: " << temp;
		this->m_vlc_media[f] = libvlc_media_new_path(this->m_vlc_inst, temp.c_str());
	}
	std::vector<std::string> dirs = raspberry_jukebox::config::get_available_subdirectories(
			config.m_config.working_directory());
	for (const std::string& dir : dirs) {
		std::string temp = config.m_config.working_directory() + dir;
		BOOST_LOG_TRIVIAL(debug)<< "Add playlist: " << temp;
		this->m_vlc_media_list[dir] = libvlc_media_list_new(this->m_vlc_inst);
		auto files = raspberry_jukebox::config::get_available_files(temp);
		for (const auto& f : *files) {
			std::string temp_file = temp + f;
			BOOST_LOG_TRIVIAL(debug)<< "Add media to list: " << temp_file;
			libvlc_media_list_add_media(this->m_vlc_media_list.at(dir),
					libvlc_media_new_path(this->m_vlc_inst, temp_file.c_str()));
		}
	}
}

std::unique_ptr<rpi_jukebox_events::RpcResult> ExecuteHandler::handle_execute(const rpi_jukebox_events::Rpc& rpc, raspberry_jukebox::config::ConfigContainer& config) {
	internals::check_rpc_id(rpi_jukebox_events::RPC_ID::RPC_EXECUTE, rpc);
	std::unique_ptr<rpi_jukebox_events::RpcResult> result = nullptr;
	if(rpc.has_event()) {
		auto element = internals::find_event_action(config.m_config.event_actions(), rpc.event().uid(), rpc.event().rfid_uid());
		if(element != std::end(config.m_config.event_actions())) {
			switch(element->action()) {
			case rpi_jukebox_events::ACTION::ACTION_PLAY:
				result = handle_action_play(rpc, config);
				break;
			case rpi_jukebox_events::ACTION::ACTION_PAUSE:
				result = handle_action_pause();
				break;
			case rpi_jukebox_events::ACTION::ACTION_FORWARD:
				result = handle_action_forward();
				break;
			case rpi_jukebox_events::ACTION::ACTION_REWIND:
				result = handle_action_rewind();
				break;
			case rpi_jukebox_events::ACTION::ACTION_STOP:
				result = handle_action_stop();
				break;
			default:
				result = std::make_unique<rpi_jukebox_events::RpcResult>();
				result->set_result(rpi_jukebox_events::RESULT::RES_ERROR);
			}
		} else {
				result = std::make_unique<rpi_jukebox_events::RpcResult>();
				result->set_result(rpi_jukebox_events::RESULT::RES_ERROR);
		}
	} else {
		result = std::make_unique<rpi_jukebox_events::RpcResult>();
		result->set_result(rpi_jukebox_events::RESULT::RES_ERROR);
	}
	return result;
}

void ExecuteHandler::stop_and_release_media_player() {
	if(this->m_vlc_media_player != nullptr) {
		libvlc_media_player_stop (this->m_vlc_media_player);
		libvlc_media_player_release (this->m_vlc_media_player);
	} else if(this->m_vlc_media_list_player != nullptr) {
		libvlc_media_list_player_stop (this->m_vlc_media_list_player);
		libvlc_media_list_player_release (this->m_vlc_media_list_player);
	}
	this->m_vlc_media_list_player = nullptr;
	this->m_vlc_media_player = nullptr;
}

void ExecuteHandler::pause_media_player() {
	if(this->m_vlc_media_player != nullptr) {
		libvlc_media_player_pause(this->m_vlc_media_player);
	} else if(this->m_vlc_media_list_player != nullptr) {
		libvlc_media_list_player_pause(this->m_vlc_media_list_player);
	}
}

void ExecuteHandler::forward_media_player() {
	if(this->m_vlc_media_list_player != nullptr) {
		libvlc_media_list_player_next(this->m_vlc_media_list_player);
	}
}

void ExecuteHandler::rewind_media_player() {
	if(this->m_vlc_media_list_player != nullptr) {
		libvlc_media_list_player_previous(this->m_vlc_media_list_player);
	}
}

int ExecuteHandler::play_media_player() {
	if(this->m_vlc_media_player != nullptr) {
		return libvlc_media_player_play(this->m_vlc_media_player);
	} else {
		libvlc_media_list_player_play(this->m_vlc_media_list_player);
		return 0;
	}
}
