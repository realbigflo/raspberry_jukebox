#pragma once

#include <exception>

namespace raspberry_jukebox {

class MessageMalformedError: public std::exception {
};

}
