#pragma once

#include "raspberry_jukebox/common.h"

// cppcheck-suppress missingInclude
#include "rpi_jukebox_events.pb.h"
// cppcheck-suppress missingInclude
#include "zmq.hpp"

#include <ostream>
#include <memory>
#include <string>

namespace raspberry_jukebox
{

class EventReader {
public:
	EventReader();
	virtual ~EventReader();

	/// Blocking call to return next event from the source
	///
	/// Can throw a StopException if the reader is going to quit reading.
	virtual std::unique_ptr<rpi_jukebox_events::Event> get_next_event();

	/// Signal the Event Reader to stop receiving events and prepare for a clean shutdown
	///
	virtual void signal_stop();
};

class RpcSender {
public:
	/// Create an EventProvider instance that will connect to the given URI.
	RpcSender(const std::string& uri);

	std::unique_ptr<rpi_jukebox_events::RpcResult> rpc(rpi_jukebox_events::RPC_ID rpc_id);
	std::unique_ptr<rpi_jukebox_events::RpcResult> rpc(rpi_jukebox_events::RPC_ID rpc_id, std::unique_ptr<rpi_jukebox_events::Event> event);
	std::unique_ptr<rpi_jukebox_events::RpcResult> rpc(rpi_jukebox_events::RPC_ID rpc_id, std::unique_ptr<rpi_jukebox_events::EventAction> event_action);
	std::string to_str() const;

private:

	std::unique_ptr<rpi_jukebox_events::RpcResult> fetch_rpc_result();
	void send_rpc(const rpi_jukebox_events::Rpc& rpc);
	zmq::context_t m_zmq_context;
	zmq::socket_t m_zmq_socket;
	const std::string m_uri;
};

std::ostream& operator<<(std::ostream& ostream, const RpcSender& provider);

}
