#include "raspberry_jukebox/common.h"
#include "raspberry_jukebox/provider.h"
#include "raspberry_jukebox/provider_errors.h"
#include <boost/program_options.hpp>
#include <mfrc522/MFRC522.h>
#include <iostream>
#include <unistd.h>

namespace rj = raspberry_jukebox; 
namespace po = boost::program_options;

int main(int argc, char **argv) {

	po::options_description desc("Allowed options");
	desc.add_options()("help,h", "produce help message")("verbose,v",
			"Print debug messages to stdout")("uri",
			po::value<std::string>()->default_value(
					raspberry_jukebox::DEFAULT_URI_CLIENT),
			"set the uri of the jukebox backend");

	po::variables_map vm;
	try {
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
		std::cerr << desc << std::endl;
		return EINVAL;
	}

	if (vm.count("help")) {
		std::cout << desc << std::endl;
		return 0;
	}

	rj::RpcSender rpc_sender(vm["uri"].as<std::string>());
	MFRC522 mfrc;

	mfrc.PCD_Init();
	std::cout << "Wait for next RFID tag" << std::endl;

	while(true) {
		// Look for card
		if(!mfrc.PICC_IsNewCardPresent() || !mfrc.PICC_ReadCardSerial()) {
				usleep(1000*500);
				continue;
		}
		uint64_t rfid_tag = 0;
		//FIXME: support 10 byte uid ?
		for(int i = 0; i < mfrc.uid.size && i < 8; ++i) {
			rfid_tag <<=8;
			rfid_tag |= mfrc.uid.uidByte[i];
		}

		auto event = std::make_unique<rpi_jukebox_events::Event>();
		event->set_rfid_uid(rfid_tag);
		std::cout << "Send: " << std::hex << event->ShortDebugString() << std::endl;
		auto result = rpc_sender.rpc(rpi_jukebox_events::RPC_ID::RPC_EXECUTE, std::move(event));
		if(result->result() != rpi_jukebox_events::RESULT::RES_OK) {
			std::cerr << "Bad response from jukebox backend: " << result->ShortDebugString() << std::endl;
		}
		usleep(1000*2000);
		std::cout << "Wait for next RFID tag" << std::endl;
	}

	return 0;
}
