#include <boost/log/core/core.hpp>
#include <boost/log/core/record.hpp>
#include <boost/log/expressions/filter.hpp>
#include <boost/log/keywords/file_name.hpp>
#include <boost/log/keywords/format.hpp>
#include <boost/log/keywords/rotation_size.hpp>
#include <boost/log/keywords/severity.hpp>
#include <boost/log/sources/basic_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/parameter/keyword.hpp>
#include <boost/program_options.hpp>
#include <pwd.h>
#include <raspberry_jukebox/common.h>
#include <raspberry_jukebox/provider.h>
#include <rpi_jukebox_config.pb.h>
#include <rpi_jukebox_events.pb.h>
#include <unistd.h>
#include <algorithm>
#include <experimental/filesystem>
#include <iterator>
#include <iostream>
#include <map>
#include <unordered_map>
#include <string>

namespace po = boost::program_options;
namespace rj = raspberry_jukebox;
namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;

src::severity_logger<logging::trivial::severity_level> *g_logger = nullptr;

#define LOG_DEBUG BOOST_LOG_SEV(*g_logger, logging::trivial::debug)
#define LOG_INFO BOOST_LOG_SEV(*g_logger, logging::trivial::info)
#define LOG_WARNING BOOST_LOG_SEV(*g_logger, logging::trivial::warning)
#define LOG_ERROR BOOST_LOG_SEV(*g_logger, logging::trivial::error)

static void logging_init(boost::log::trivial::severity_level level) {
	boost::log::add_console_log(std::cout, boost::log::keywords::format =
			">> %Message%");
	logging::add_common_attributes();
	logging::core::get()->set_filter(logging::trivial::severity >= level);
	g_logger = new src::severity_logger<logging::trivial::severity_level>();
}

static void register_and_execute(rj::RpcSender& rpc_sender,
		const std::string& what, rpi_jukebox_events::ACTION action) {
	auto result = rpc_sender.rpc(
			rpi_jukebox_events::RPC_ID::RPC_QUERY_REGISTERED);
	auto cmp = [&what](const rpi_jukebox_events::EventAction& ea) {
		return ea.event().uid() == what;
	};
	if (result->event_actions().cend()
			== std::find_if(std::cbegin(result->event_actions()),
					std::cend(result->event_actions()), cmp)) {
		auto ea = std::make_unique<rpi_jukebox_events::EventAction>();
		ea->mutable_event()->set_uid(what);
		ea->set_action(action);
		rpc_sender.rpc(rpi_jukebox_events::RPC_ID::RPC_REGISTER, std::move(ea));
	}
	auto event = std::make_unique<rpi_jukebox_events::Event>();
	event->set_uid(what);
	result = rpc_sender.rpc(rpi_jukebox_events::RPC_ID::RPC_EXECUTE,
			std::move(event));
	if (result->result() != rpi_jukebox_events::RESULT::RES_OK) {
		std::cerr << "Cannot execute \" "<< what <<" \" for some reason" << std::endl;
	}
}

static uint64_t rj_stoll(const std::string& uid) {
	std::size_t pos;
	auto rfid_uid = static_cast<uint64_t>(std::stoll(uid, &pos, 16));
	if(pos != uid.size()) {
		throw std::invalid_argument("Not a complete number");
	}
	return rfid_uid;
}

static std::unique_ptr<rpi_jukebox_events::EventAction> create_event_action_with_uid(const std::string& uid) {
	auto ea = std::make_unique<rpi_jukebox_events::EventAction>();
	try {
		auto rfid_uid = rj_stoll(uid);
		ea->mutable_event()->set_rfid_uid(rfid_uid);
	}catch(std::invalid_argument& ) {
		ea->mutable_event()->set_uid(uid);
	}
	return ea;
}

int main(int argc, char **argv) {

	struct passwd *pw = getpwuid(getuid());
	const std::string homedir(pw->pw_dir);

	po::options_description desc("Allowed options");
	desc.add_options()("help,h", "produce help message")("verbose,v",
			"Print debug messages to stdout")("uri",
			po::value<std::string>()->default_value(
					raspberry_jukebox::DEFAULT_URI_CLIENT),
			"set the uri of the jukebox backend");
	po::options_description rpcs("Remote Procedure Calls");
	rpcs.add_options()("query_registered,q", "Query registered actions")(
			"query_available,a",
			"Query available music folder")("query_available_files", "Query available music files")("stop,s",
			"Stop music")("pause", "Pause music")("forward,f", "Next track")(
			"rewind,r", "Rewind track")("play,p", po::value<std::string>(),
			"Play the given file")
			("register",
			po::value<std::string>(),
			"Register play event for an available file in form: <UID>,<filename>. Fails if UID is already in use and --update is not given. Fails if filename is not available. If UID starts with \"0x\", the id is treated as RFID UID. Special cases: instead of filename following actions are accepted: FORWARD,REWIND,STOP,PAUSE")
			("update", "Updates the filename register to a certain UID if present.")
			("delete", po::value<std::string>(), "Delete the action for the given uid");


	desc.add(rpcs);

	po::variables_map vm;
	try {
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
		std::cerr << desc << std::endl;
		return EINVAL;
	}

	if (vm.count("help")) {
		std::cout << desc << std::endl;
		return 0;
	}
	logging_init(
			vm.count("verbose") ?
					logging::trivial::debug : logging::trivial::warning);

	LOG_DEBUG<< "Connect to: " << vm["uri"].as<std::string>();

	rj::RpcSender rpc_sender(vm["uri"].as<std::string>());

	if(vm.count("register")) {
		auto reg_op = vm["register"].as<std::string>();
		const auto pos = reg_op.find(",");
		if(pos == std::string::npos) {
			LOG_ERROR << "Invalid option \"--register\". Usage: <UID>,<filename> ";
			return -1;
		}
		auto uid = reg_op.substr(0,pos);
		auto filename = reg_op.substr(pos+1);
		LOG_DEBUG << "Try to register " << uid << " - " << filename;

		std::unordered_map<std::string, rpi_jukebox_events::ACTION> extra_actions { { "FORWARD",
				rpi_jukebox_events::ACTION::ACTION_FORWARD }, { "REWIND", rpi_jukebox_events::ACTION::ACTION_REWIND }, {
				"STOP", rpi_jukebox_events::ACTION::ACTION_STOP }, { "PAUSE", rpi_jukebox_events::ACTION::ACTION_PAUSE } };
		std::unordered_map<std::string, rpi_jukebox_events::ACTION>::const_iterator extra_action_it =
				extra_actions.find(filename);
		if (extra_action_it == extra_actions.cend()) {
			auto available = rpc_sender.rpc(
							rpi_jukebox_events::RPC_ID::RPC_QUERY_AVAILABLE);

			auto cmp = [&filename](const std::string& av){
				return av == filename;
			};
			if(std::find_if(std::cbegin(available->available_files()), std::cend(available->available_files()), cmp) == std::cend(available->available_files())){
				auto available_files = rpc_sender.rpc(rpi_jukebox_events::RPC_ID::RPC_QUERY_AVAILABLE_FILES);
				if (std::find_if(std::cbegin(available_files->available_files()),
						std::cend(available_files->available_files()), cmp)
						== std::cend(available_files->available_files())) {
					LOG_ERROR<< filename << " is not available in jukebox";
					return EINVAL;
				}
			}
		}

		if(vm.count("update") == 0) {
			auto registered = rpc_sender.rpc(
					rpi_jukebox_events::RPC_ID::RPC_QUERY_REGISTERED);
			try {
				auto rfid_uid = rj_stoll(uid);
				auto cmp = [&rfid_uid](const auto& av){
					return av.event().rfid_uid() == rfid_uid;
				};
				if(std::find_if(std::cbegin(registered->event_actions()), std::cend(registered->event_actions()), cmp) != std::cend(registered->event_actions())){
					LOG_ERROR << uid << " already in use. Use --update to override";
					return EINVAL;
				}
			} catch(std::invalid_argument& ) {
				auto cmp = [&uid](const auto& av){
					return av.event().uid() == uid;
				};
				if(std::find_if(std::cbegin(registered->event_actions()), std::cend(registered->event_actions()), cmp) != std::cend(registered->event_actions())){
					LOG_ERROR << uid << " already in use. Use --update to override";
					return EINVAL;
				}
			}
		}
		auto ea = create_event_action_with_uid(uid);
		if (extra_action_it == extra_actions.cend()) {
			ea->set_action(rpi_jukebox_events::ACTION::ACTION_PLAY);
			ea->set_filename(filename);
		} else {
			ea->set_action(extra_action_it->second);
		}
		rpc_sender.rpc(rpi_jukebox_events::RPC_ID::RPC_REGISTER, std::move(ea));
	}

	if(vm.count("delete")) {
		auto uid = vm["delete"].as<std::string>();
		auto ea = create_event_action_with_uid(uid);
		LOG_DEBUG << "Try to delete : " << uid;
		rpc_sender.rpc(rpi_jukebox_events::RPC_ID::RPC_DELETE, std::move(ea));
	}

	if (vm.count("stop")) {
		register_and_execute(rpc_sender, "STOP",
				rpi_jukebox_events::ACTION::ACTION_STOP);
	}
	if (vm.count("pause")) {
		register_and_execute(rpc_sender, "PAUSE",
				rpi_jukebox_events::ACTION::ACTION_PAUSE);
	}
	if (vm.count("rewind")) {
		register_and_execute(rpc_sender, "REWIND",
				rpi_jukebox_events::ACTION::ACTION_REWIND);
	}
	if (vm.count("forward")) {
		register_and_execute(rpc_sender, "FORWARD",
				rpi_jukebox_events::ACTION::ACTION_FORWARD);
	}
	if (vm.count("query_registered")) {
		auto result = rpc_sender.rpc(
				rpi_jukebox_events::RPC_ID::RPC_QUERY_REGISTERED);
		std::cout << "Registered Actions: " << std::endl;
		if (result->event_actions().size() == 0) {
			std::cout << "No registered action" << std::endl;
		}
		for (const auto &ea : result->event_actions()) {
			std::cout << "[Action]: ";

			if (ea.event().rfid_uid() != 0) {
				std::cout << "RFID UID: 0x" << std::hex << ea.event().rfid_uid()
						<< " ";
			}
			if (ea.event().uid() != std::string()) {
				std::cout << "UID: " << ea.event().uid() << " ";
			}
			std::cout << rj::get_action_name_for_id(ea.action()) << " ";
			std::cout << ea.working_directory() << " " << ea.filename()
					<< std::endl;
		}
	}
	if (vm.count("query_available")) {
		auto result = rpc_sender.rpc(
				rpi_jukebox_events::RPC_ID::RPC_QUERY_AVAILABLE);
		std::cout << "Available music folders: " << std::endl;
		if (result->result() != rpi_jukebox_events::RESULT::RES_OK) {
			std::cerr << "RPC to jukebox went because of unknown reasons" << std::endl;
		} else {
			if (result->available_files_size() == 0) {
				std::cout << "No music file available on jukebox" << std::endl;
			} else {
				for (const auto &filename : result->available_files()) {
					std::cout << filename << std::endl;
				}
			}
		}
	}
	if (vm.count("query_available_files")) {
		auto result = rpc_sender.rpc(rpi_jukebox_events::RPC_ID::RPC_QUERY_AVAILABLE_FILES);
		std::cout << "Available music files: " << std::endl;
		if (result->result() != rpi_jukebox_events::RESULT::RES_OK) {
			std::cerr << "RPC to jukebox went because of unknown reasons"
					<< std::endl;
		} else {
			if (result->available_files_size() == 0) {
				std::cout << "No music file available on jukebox" << std::endl;
			} else {
				for (const auto &filename : result->available_files()) {
					std::cout << filename << std::endl;
				}
			}
		}
	}
	if (vm.count("play") > 0) {
		auto event = std::make_unique<rpi_jukebox_events::Event>();
		auto uid = vm["play"].as<std::string>();
		try {
			auto rfid_uid = rj_stoll(uid);
			event->set_rfid_uid(rfid_uid);
		} catch(const std::invalid_argument& ) {
			std::cerr << "No number" << std::endl;
			event->set_uid(uid);
		}
		auto result = rpc_sender.rpc(rpi_jukebox_events::RPC_ID::RPC_EXECUTE,
				std::move(event));
		if (result->result() != rpi_jukebox_events::RESULT::RES_OK) {
			std::cerr << "Wanted uid not registered" << std::endl;
		}
	}

	LOG_DEBUG<< "Finished work !";
	return 0;
}
